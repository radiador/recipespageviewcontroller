//
//  RecipesPageViewController.swift
//  RecipesPageViewController
//
//  Created by formador on 21/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class RecipesPageViewController: UIPageViewController {
    
    var recipes = [Recipe]()
    private var recipesViewControllers = [Int: RecipeViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        recipes.append(Recipe(name: "Fabada", image: UIImage(named: "receta1.jpeg") ?? UIImage()))
        recipes.append(Recipe(name: "Cocido", image: UIImage(named: "receta2.jpg") ?? UIImage()))
        recipes.append(Recipe(name: "Pasta", image: UIImage(named: "receta3.jpg") ?? UIImage()))
        recipes.append(Recipe(name: "Arroz", image: UIImage(named: "receta4.jpg") ?? UIImage()))
        recipes.append(Recipe(name: "Lentejas", image: UIImage(named: "receta5.jpeg") ?? UIImage()))

        dataSource = self
        
        let recipeViewController = getRecipeViewController(forIndex: 0)
        
        setViewControllers([recipeViewController], direction: .forward, animated: false, completion: nil)

        /*
         Solo despues de que iOS considere que va a usar el view controller instanciado (por ejemplo añadiendolo a un pageViewController, en un prepareForSeguue o llamando al view), se asociarans sus outlets
         */
        recipeViewController.configureRecipe(recipe: recipes[0])

    }
    
    
    private func createRecipeViewController() -> RecipeViewController {
        
        let recipeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "recipeVCIdentifier") as? RecipeViewController
        
        return recipeVC ?? RecipeViewController()
    }
    
    private func getRecipeViewController(forIndex index: Int) -> RecipeViewController {
        
        var recipeViewController = recipesViewControllers[index]
        
        if recipeViewController == nil {
            
            recipeViewController = createRecipeViewController()
            
            recipesViewControllers[index] = recipeViewController
        }
        
        return recipeViewController!
    }
}

extension RecipesPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        return nil
    }
    

}
