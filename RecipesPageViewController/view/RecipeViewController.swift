//
//  RecipeViewController.swift
//  RecipesPageViewController
//
//  Created by formador on 21/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    func configureRecipe(recipe: Recipe) {
        
        imageView.image = recipe.image
        
        label.text = recipe.name
    }
}
